public with sharing class AccountContactTriggerHandler {

    public static Boolean IsRunning = false;

    public void run() {
        List<AccountContact__c> newReferences = (List<AccountContact__c>) Trigger.new;
        Map<Id, AccountContact__c> oldReferences = (Map<Id, AccountContact__c>) Trigger.oldMap;
    
        Set<Id> contactIds = new Set<Id>();
        for (AccountContact__c reference : newReferences) {
            contactIds.add(reference.Contact__c);
        }
    
        if (Trigger.isInsert) {
            List<Contact> contacts = [
                SELECT (SELECT Id FROM AccountContacts__r LIMIT 1) 
                FROM Contact
                WHERE Id IN :contactIds
            ];
            Set<Id> contactIdsWithoutReferences = new Set<Id>();
            for (Contact contact : contacts) {
                if (contact.AccountContacts__r.isEmpty()) {
                    contactIdsWithoutReferences.add(contact.Id);
                }
            }
    
            for (AccountContact__c reference : newReferences) {
                if (contactIdsWithoutReferences.contains(reference.Contact__c)) {
                    reference.IsPrimary__c = true;
                }
            }
        } else if (Trigger.isUpdate) {
            List<Contact> contacts = [
                SELECT (SELECT Id FROM AccountContacts__r ORDER BY IsPrimary__c DESC, CreatedDate DESC LIMIT 2) 
                FROM Contact
                WHERE Id IN :contactIds
            ];
            Map<Id, Contact> contactsMap = new Map<Id, Contact>(contacts);
    
            List<AccountContact__c> referencesToUpdate = new List<AccountContact__c>();
            for (AccountContact__c reference : newReferences) {
                AccountContact__c oldReference = oldReferences.get(reference.Id);
    
                if (reference.IsPrimary__c != oldReference.IsPrimary__c) {
                    if (reference.IsPrimary__c) {
                        AccountContact__c lastReference = contactsMap.get(reference.Contact__c).AccountContacts__r.get(0);
                        lastReference.IsPrimary__c = false;
                        referencesToUpdate.add(lastReference);
                    } else {
                        AccountContact__c lastReference;
                        List<AccountContact__c> contactRefs = contactsMap.get(reference.Contact__c).AccountContacts__r;
                        if (contactRefs.size() > 1) {
                            lastReference = contactRefs.get(1);
                        } else {
                            lastReference = contactRefs.get(0);
                        }
                        lastReference.IsPrimary__c = true;
                        referencesToUpdate.add(lastReference);
                    }
                }
            }
    
            IsRunning = true;
            update referencesToUpdate;
            IsRunning = false;
        }
    }
}
