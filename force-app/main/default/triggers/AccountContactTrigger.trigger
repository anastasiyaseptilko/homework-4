trigger AccountContactTrigger on AccountContact__c (before insert, before update) {
    if (!AccountContactTriggerHandler.IsRunning) {
        new AccountContactTriggerHandler().run();
    }
}